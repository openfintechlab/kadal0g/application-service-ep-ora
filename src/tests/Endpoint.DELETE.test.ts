import util                     from "util";
import OracleDBInteractionUtil  from '../utils/OracleDBInteractionUtil';
import {Main}                   from "../index";
import AppConfig                from "../config/AppConfig";
import logger                   from "../utils/Logger";
import axios                    from "axios";
import chalk                    from "chalk";
import UTSetup                  from "./setup_express";



describe(`POST Endpoint Vlaidation Test Cases`, () => {
    const post_message = require(`./POST.message.json`);    
    const delete_message = require(`./DELETE.message.json`);


    beforeAll(async () => {        
        try{
            logger.info(`Package Name: ${AppConfig.package_name}`)
            await Main.start();            
            await OracleDBInteractionUtil.initPoolConnectionsWithConfig(); 
            // Create a record in the database
            // const url = `http://localhost:${AppConfig.config.OFL_MED_PORT}/${AppConfig.package_name}`
            const url = UTSetup.getURL();
            logger.info(`Calling URL: ${url}`);
            const response = await axios({
                "method": "post",
                "url": url,
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"},
                "data": post_message
            });
            const statusCode:number = response.status;
            expect(statusCode === 200 || statusCode === 201).toBeTruthy();            
        }catch(error){
            logger.error(`Error received while Bootstarting the solution: ${error}`);
            logger.error(`Unable to proceed. Closing application`);                   
        }            
    });

    afterAll(async ()=> {
        await OracleDBInteractionUtil.closePoolAndExit(); 
        await Main.expressApp.server.close();
    });

    test(`DELETE ${AppConfig.package_name} to the backend service-plane`, async () => {   
        const appSrvEp = post_message['application-service-ep'];             
        const url = UTSetup.getURL() + `/${appSrvEp.application_id}` + `?` + `srv_id=${appSrvEp.service_id}&ver_id=${appSrvEp.version_id}&ep_id=${appSrvEp.endpoint_id}`;
        logger.info(chalk.bgGray(`Calling URL: ${url}`));
        try{    
            const response = await axios.delete( url,{
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"}
            });
            const statusCode:number = response.status;
            expect(statusCode === 200 || statusCode === 201).toBeTruthy();                        
        }catch(error){
            if(error.response){
                logger.error(chalk.bgYellow(`${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`))
                expect(error.response.status).not.toBe(500);
            }else{                
                fail(error.message)                
            }            
        }        
    });

    test(`DELETE ${AppConfig.package_name} again to the backend service-plane. Expect error code 404`, async () => {        
        // const url = `http://localhost:${AppConfig.config.OFL_MED_PORT}/${AppConfig.package_name}/${delete_message.id}`
        const appSrvEp = post_message['application-service-ep'];             
        const url = UTSetup.getURL() + `/${appSrvEp.application_id}` + `?` + `srv_id=should_not_exist&ver_id=${appSrvEp.version_id}&ep_id=${appSrvEp.endpoint_id}`;
        logger.info(chalk.bgGray(`Calling URL: ${url}`));
        try{    
            const response = await axios.delete( url,{
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"}
            });
            const statusCode:number = response.status;
            expect(statusCode === 200 || statusCode === 201).not.toBeTruthy();                        
            // Now create it
        }catch(error){
            if(error.response){
                logger.error(chalk.bgYellow(`${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`))
                expect(error.response.status).toBe(404);
            }else{                
                fail(error.message)                
            }            
        }        
    });

    test(`DELETE non-existential ${AppConfig.package_name}  to the backend service-plane. Expect error code 404`, async () => {        
        // const url = `http://localhost:${AppConfig.config.OFL_MED_PORT}/${AppConfig.package_name}/should_not_exist`
        const url = UTSetup.getURL() + `/should_not_exist`;
        logger.info(chalk.bgGray(`Calling URL: ${url}`));
        try{    
            const response = await axios.delete( url,{
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"}
            });
            const statusCode:number = response.status;
            expect(statusCode === 200 || statusCode === 201).not.toBeTruthy();                        
            // Now create it
        }catch(error){
            if(error.response){
                logger.error(chalk.bgYellow(`${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`))
                expect(error.response.status).toBe(404);
            }else{                
                fail(error.message)                
            }            
        }        
    });

});