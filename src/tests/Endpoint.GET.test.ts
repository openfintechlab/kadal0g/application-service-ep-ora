import util                     from "util";
import OracleDBInteractionUtil  from '../utils/OracleDBInteractionUtil';
import {Main}                   from "../index";
import AppConfig                from "../config/AppConfig";
import logger                   from "../utils/Logger";
import axios                    from "axios";
import chalk                    from "chalk";
import UTSetup                  from "./setup_express";




describe(`GET Endpoint Validation Test Cases`, () => {
    const post_message = require(`./POST.message.json`);
    const delete_message = require(`./DELETE.message.json`);
    
    beforeAll(async () => {        
        try{
            logger.info(`Package Name: ${AppConfig.package_name}`)
            await Main.start();            
            await OracleDBInteractionUtil.initPoolConnectionsWithConfig(); 
            // DELETE            
            const url = UTSetup.getURL();
            logger.info(`Calling URL: ${url}`);
            const response = await axios({
                "method": "post",
                "url": url,
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"},
                "data": post_message
            });
            const statusCode:number = response.status;
            expect(statusCode === 200 || statusCode === 201).toBeTruthy();            
        }catch(error){
            logger.error(`Error received while Bootstarting the solution: ${error}`);
            logger.error(`Unable to proceed. Closing application`);       
            // process.exit(1);            
        }            
    });
    
    afterAll(async ()=> {
        // Delete specific record
        try{
            // const url = `http://localhost:${AppConfig.config.OFL_MED_PORT}/${AppConfig.package_name}/${delete_message.id}`            
            const appSrvEp = post_message['application-service-ep'];
            const url = UTSetup.getURL() + `/${appSrvEp.application_id}` + `?` + `srv_id=${appSrvEp.service_id}&ver_id=${appSrvEp.version_id}&ep_id=${appSrvEp.endpoint_id}`;
            logger.info(`+ Cleaning created records using url: ${url}`);
            const response = await axios.delete( url,{
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"}
            });
        }catch(error){
            if(error.response){
                logger.error(chalk.bgYellow(`${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`))
            }else{
                logger.error(chalk.red(`Error while cleaning specific record`))
            }           
            fail(error.message);
        }
        await OracleDBInteractionUtil.closePoolAndExit(); 
        await Main.expressApp.server.close();
    });


    test(`Fetch created record ${AppConfig.package_name} from the backend service-plane`, async () => {                
        const url = UTSetup.getURL() + `/${post_message["application-service-ep"].application_id}`;
        logger.info(chalk.bgGray(`Calling URL: ${url}`));
        try{    
            const response = await axios.get( url,{
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"}
            });
            const statusCode:number = response.status;
            expect(response.data["application-service-ep"]).toBeDefined();
            expect(response.data.metadata.status).toBe(`0000`);
            expect(statusCode === 200 || statusCode === 201 ).toBeTruthy();
        }catch(error){
            if(error.response){
                logger.error(chalk.yellow(`${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`))                
            }
            fail(error.message)                
        }        
    });

    test(`Fetch specific ${AppConfig.package_name} from the backend service-plane. Should not Exist`, async () => {        
        // const url = `http://localhost:${AppConfig.config.OFL_MED_PORT}/${AppConfig.package_name}/should_not_exist`
        const url = UTSetup.getURL() + '/shoudl_not_exist';
        logger.info(chalk.bgGray(`Calling URL: ${url}`));
        try{    
            const response = await axios.get( url,{
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"}
            });
            const statusCode:number = response.status;
            expect(statusCode === 200 || statusCode === 201).not.toBeTruthy();
        }catch(error){
            if(error.response){
                logger.info(chalk.yellow(`${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`))
                expect(error.response.status).toBe(404);
                expect(error.response.data.metadata.status).toBeDefined();
            }else{                
                fail(error.message)                
            }            
        }        
    });

    test(`Fetch ${AppConfig.package_name} for application_id, version_id, service_id and endpoint_id. Specific ID`, async () => {        
        // const url = `http://localhost:${AppConfig.config.OFL_MED_PORT}/should_not_exist`
        const appSrvEp = post_message['application-service-ep'];
        const url = UTSetup.getURL() + `/${appSrvEp.application_id}` + `?` + `srv_id=${appSrvEp.service_id}&ver_id=${appSrvEp.version_id}&ep_id=${appSrvEp.endpoint_id}`;
        logger.info(chalk.bgGray(`Calling URL: ${url}`));
        try{    
            const response = await axios.get( url,{
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"}
            });
            const statusCode:number = response.status;
            expect(statusCode === 200 || statusCode === 201).toBeTruthy();
        }catch(error){
            if(error.response){
                logger.info(chalk.bgRedBright(`${error.response.status} .::. ${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`))                
            }
            fail(error.message);
        }        
    });

});