import OracleDBInteractionUtil          from "..//utils/OracleDBInteractionUtil";
import util                             from "util";
import { v4 as uuidv4 }                 from "uuid";
import  logger                          from "../utils/Logger";
import {PostRespBusinessObjects}        from "./bussObj/Response";


export default class ApplicationSrvEpController{
    SQLSstatements = {
        "SELECT_SQL01_ONPK": "SELECT application_id from application_service_ep where APPLICATION_ID=:v1 AND SERVICE_ID=:v2 AND VERSION_ID=:v3 AND ENDPOINT_ID=:v4",
        "INSERT_SQL02_ALL": "INSERT INTO application_service_ep(application_id,service_id,version_id,endpoint_id,created_on,updated_on,created_by,updated_by) VALUES(:v1,:v2,:v3,:v4,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'SYSTEM', 'SYSTEM')",
        "SELECT_SQL03_ONIDS": 'select ase.application_id,ase.service_id,ase.version_id,ase.endpoint_id, msa.name as app_name, msa.status as app_status,msa.status_description, ms.name as service_name, ms.title as srv_title ' +
                                    'from application_service_ep ase ' +
                                    'INNER JOIN med_service_application msa '+
                                            'on ase.application_id = msa.application_id '+
                                    'INNER JOIN med_service ms '+
                                            'on ase.service_id = ms.srv_id ' +
                                    'WHERE ase.application_id= COALESCE(:v1, ase.application_id) AND '+
                                            'ase.service_id = COALESCE(:v2,ase.service_id) AND '+
                                            'ase.version_id = COALESCE(:v3,ase.version_id) AND '+
                                            'ase.endpoint_id = COALESCE(:v4, ase.endpoint_id) ' +
                                    'ORDER BY msa.created_on desc ',                                    
        "DELETE_SQL04_ONIDS": "DELETE FROM application_service_ep where APPLICATION_ID=:v1 AND SERVICE_ID=COALESCE(:v2,SERVICE_ID) AND VERSION_ID=COALESCE(:v3,VERSION_ID) AND ENDPOINT_ID=COALESCE(:v4,ENDPOINT_ID)",               
        "SELECT_SQL05_ONIDS_SLIM": "SELECT application_id, service_id, version_id, endpoint_id from application_service_ep WHERE application_id= COALESCE(:v1, application_id) AND service_id = COALESCE(:v2,service_id) AND version_id = COALESCE(:v3,version_id) AND endpoint_id = COALESCE(:v4, endpoint_id)"
                            
    }
    
    constructor(private referenceID: string){}

    /**
     * Creates a new Applicatio-Service-endpoint controller
     * @param {obj} request Request object
     */
    public async create(request: any){
        logger.debug(`Request Received for creating application`, `${this.referenceID}`, `${util.inspect(request,{compact:true,colors:true, depth: null})}`);        
        try{
            const jsnAppSrvEp = request["application-service-ep"];
            let result = await OracleDBInteractionUtil.executeRead(this.SQLSstatements.SELECT_SQL01_ONPK, [jsnAppSrvEp.application_id,jsnAppSrvEp.service_id,jsnAppSrvEp.version_id,jsnAppSrvEp.endpoint_id])
            if(result.rows.length > 0){
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8152","Application already exist");
            }
            const binds =[jsnAppSrvEp.application_id,jsnAppSrvEp.service_id,jsnAppSrvEp.version_id,jsnAppSrvEp.endpoint_id];
            const dbResult = await OracleDBInteractionUtil.executeUpdate(this.SQLSstatements.INSERT_SQL02_ALL,binds);
            return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!");
        }catch(error){
            logger.error(`Error adding application in the db`, `${this.referenceID}`, `${util.inspect(error,{compact:true,colors:true, depth: null})}`);
            if(error.metadata){
                throw error;
            }else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8503","Error While performing the Operation");
            }            
        }
    }

    /**
     * 
     * @param {string} app_id Application ID (mandatory)
     * @param {string} srv_id Service ID (optional)
     * @param {string} ver_id Version ID (optional)
     * @param {string} ep_id  Endpoint ID (optional)
     */
    public async getOnIds(app_id:string,srv_id?: string, ver_id?: string, ep_id?: string){
        try{
            let dbresult = await OracleDBInteractionUtil.executeRead(this.SQLSstatements.SELECT_SQL03_ONIDS,[app_id,srv_id,ver_id,ep_id]);
            return await this.createBusinessObject(dbresult);
        }catch(error){
            logger.error(`Error adding application in the db`, `${this.referenceID}`, `${util.inspect(error,{compact:true,colors:true, depth: null})}`);
            if(error.metadata){
                throw error;
            }else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8503","Error While performing the Operation");
            }            
        }
    }

    /**
     * Get authority records from the db
     * @param {string} app_id Application ID (mandatory)
     * @param {string} srv_id Service ID (optional)
     * @param {string} ver_id Version ID (optional)
     * @param {string} ep_id  Endpoint ID (optional)
     */
    public async getOnIds_Slim(app_id:string,srv_id?: string, ver_id?: string, ep_id?: string){
        try{
            let dbresult = await OracleDBInteractionUtil.executeRead(this.SQLSstatements.SELECT_SQL05_ONIDS_SLIM,[app_id,srv_id,ver_id,ep_id]);
            return await this.createBusinessObject(dbresult);
        }catch(error){
            logger.error(`Error adding application in the db`, `${this.referenceID}`, `${util.inspect(error,{compact:true,colors:true, depth: null})}`);
            if(error.metadata){
                throw error;
            }else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8503","Error While performing the Operation");
            }            
        }
    }

    /**
     * 
     * @param {string} app_id Application ID (mandatory)
     * @param {string} srv_id Service ID (optional)
     * @param {string} ver_id Version ID (optional)
     * @param {string} ep_id  Endpoint ID (optional)
     */
    public async delete(app_id:string,srv_id?: string, ver_id?: string, ep_id?: string){
        try{
            let dbresult = await OracleDBInteractionUtil.executeUpdate(this.SQLSstatements.DELETE_SQL04_ONIDS,[app_id,srv_id,ver_id,ep_id]);
            if(dbresult.rowsAffected > 0){
                return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!");   
            }                
            else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8153","Required key does not exist in the db");   
            }
        }catch(error){
            logger.error(`Error adding application in the db`, `${this.referenceID}`, `${util.inspect(error,{compact:true,colors:true, depth: null})}`);
            if(error.metadata){
                throw error;
            }else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8503","Error While performing the Operation");
            }            
        }
    }



    private async createBusinessObject(dbResult:any){
        return new Promise<any>((resolve:any, reject:any) => {
            if(dbResult.rows.length <= 0){
                reject(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8153","Required key does not exist in the db"));
            }
            var bussObj:any = {
                metadata: {
                    "status": '0000',
                    "description": 'Success!',
                    "responseTime": new Date(),                    
                },
                "application-service-ep": []
            };
            dbResult.rows.forEach((row:any) => {
                bussObj["application-service-ep"].push({
                    "application_id": row.APPLICATION_ID,
                    "service_id": row.SERVICE_ID,
                    "version_id": row.VERSION_ID,
                    "endpoint_id": row.ENDPOINT_ID,
                    "app_name": row.APP_NAME,
                    "app_status": row.APP_STATUS,
                    "app_status_desc": row.STATUS_DESCRIPTION,
                    "service_name": row.SERVICE_NAME,
                    "service_title": row.SERVICE_TITLE
                });
            });
            resolve(bussObj);
        });
    }
}