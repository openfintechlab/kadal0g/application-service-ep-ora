/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Root enry level file forr bootstarting node js application
 */

import express                              from "express";
import {AppHealth}                          from "../config/AppConfig";
import logger                               from "../utils/Logger";
import { v4 as uuidv4 }                     from "uuid";
import {PostRespBusinessObjects}            from "../mapping/bussObj/Response";
import util                                 from "util";
import ApplicationServiceEndpointValidator  from "../mapping/validators/ApplicationServiceEndpointValidator";
import ApplicationSrvEpController           from "../mapping/ApplicationSrvEpController";


const router: any = express.Router();

/**
 * Selects application-endpoint details based on multiple criterias
 * app_id: (mandatory)
 * @param {string} app_id (mandatory) Mandatory parameter
 * @param {string} srv_id (optional) Service ID 
 * @param {string} ver_id (optional) Service Version ID
 * @param {string} ep_id (optional) Service enpodint ID
 * @example
 * curl -X GET http://{HOST}:{PORT}/application-serivce-ep/{APP_ID}
 * @async 
 */
router.get(`/:app_id`, async (request:any,response:any) => {
    let transid:string = uuidv4();    
    response.set("Content-Type","application/json; charset=utf-8");    
    let app_id:string = request.params.app_id;        
    let srv_id: string = (request.query.srv_id)? request.query.srv_id : undefined;
    let ver_id: string = (request.query.ver_id)? request.query.ver_id : undefined;
    let ep_id: string = (request.query.ep_id)? request.query.ep_id : undefined;
    try{
        logger.debug(`Params received`, `${util.inspect(request.query,{compact:true,colors:true, depth: null})}`);
        new ApplicationServiceEndpointValidator(transid).validateParams(request.query);                
        let resBO = await new ApplicationSrvEpController(transid).getOnIds(app_id,srv_id,ver_id,ep_id);        
        response.status(200).send(resBO);        
    }catch(error){
        logger.debug(`Error occured while fecthing applications endpoints`,`GET /:app_id` , `${transid}`, `${util.inspect(error,{compact:true,colors:true, depth: null})}`);
        response.status(getErrRespCode(error));        
        if(error.metadata){            
            response.send(error);
        }else{                        
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9901","Error while processing the request"));
        }     
    } 
});

/**
 * Selects application-endpoint details based on multiple criterias
 * app_id: (mandatory)
 * @param {string} app_id (mandatory) Mandatory parameter
 * @param {string} srv_id (optional) Service ID 
 * @param {string} ver_id (optional) Service Version ID
 * @param {string} ep_id (optional) Service enpodint ID
 * @example
 * curl -X GET http://{HOST}:{PORT}/application-serivce-ep/{APP_ID}
 * @async 
 */
router.get(`/slim/:app_id`, async (request:any,response:any) => {
    let transid:string = uuidv4();    
    response.set("Content-Type","application/json; charset=utf-8");    
    let app_id:string = request.params.app_id;        
    let srv_id: string = (request.query.srv_id)? request.query.srv_id : undefined;
    let ver_id: string = (request.query.ver_id)? request.query.ver_id : undefined;
    let ep_id: string = (request.query.ep_id)? request.query.ep_id : undefined;
    try{
        logger.debug(`Params received`, `${util.inspect(request.query,{compact:true,colors:true, depth: null})}`);
        new ApplicationServiceEndpointValidator(transid).validateParams(request.query);                
        let resBO = await new ApplicationSrvEpController(transid).getOnIds_Slim(app_id,srv_id,ver_id,ep_id);        
        response.status(200).send(resBO);        
    }catch(error){
        logger.debug(`Error occured while fecthing applications endpoints`,`GET /:app_id` , `${transid}`, `${util.inspect(error,{compact:true,colors:true, depth: null})}`);
        response.status(getErrRespCode(error));        
        if(error.metadata){            
            response.send(error);
        }else{                        
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9901","Error while processing the request"));
        }     
    } 
});

/**
 * Creates appoication service endpoint
 * @example
 * curl -X POST http://{HOST}:{PORT}/application-serivce-ep/ --data {JSON_DATA}
 * @async
 */
router.post(`/`, async(request:any, response:any)=>{
    let transid:string = uuidv4();
    logger.debug(`Calling http-post for posting data`, `${transid}`, `${util.inspect(request.data,{compact:true,colors:true, depth: null})}`);
    response.set("Content-Type","application/json; charset=utf-8");     
    logger.info(`${transid}: Procedure called for creating application address`);             
    try{
        // TODO! Parse the request and creates specific authority record in the data store
        let parsedObj = new ApplicationServiceEndpointValidator(transid).validate(request.body);
        logger.debug(`Parsed response from validation`, `${transid}`, `${util.inspect(parsedObj,{compact:true,colors:true, depth: null})}`);
        const resBO = await new ApplicationSrvEpController(transid).create(parsedObj);        
        response.status(201).send(resBO);        
    }catch(error){
        logger.debug(`Error occured while creating applications endpoints`,`GET /:app_id` ,`POST /`,  `${transid}`, `${util.inspect(error,{compact:true,colors:true, depth: null})}`);
        response.status(getErrRespCode(error));        
        if(error.metadata){            
            response.send(error);
        }else{                        
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9901","Error while processing the request"));
        }     
    }    
});


/**
 * Deletes application-endpoint details based on multiple criterias
 * app_id: (mandatory)
 * @param {string} app_id (mandatory) Mandatory parameter
 * @param {string} srv_id (optional) Service ID 
 * @param {string} ver_id (optional) Service Version ID
 * @param {string} ep_id (optional) Service enpodint ID
 * @example
 * curl -X GET http://{HOST}:{PORT}/application-serivce-ep/{APP_ID}
 * @async
 */
router.delete(`/:app_id`, async(request:any, response:any)=>{
    let transid:string = uuidv4();
    logger.debug(`${transid}: Calling http-delete for deleting application-address with id: ${request.params.app_id}`);
    response.set("Content-Type","application/json; charset=utf-8");    
    logger.info(`${transid}: Procedure called for deleting application`);                  
    let app_id:string = request.params.app_id;        
    let srv_id: string = (request.query.srv_id)? request.query.srv_id : undefined;
    let ver_id: string = (request.query.ver_id)? request.query.ver_id : undefined;
    let ep_id: string = (request.query.ep_id)? request.query.ep_id : undefined;

    try{
        logger.debug(`Params received`, `${util.inspect(request.query,{compact:true,colors:true, depth: null})}`);
        new ApplicationServiceEndpointValidator(transid).validateParams(request.query);                
        let resBO = await new ApplicationSrvEpController(transid).delete(app_id,srv_id,ver_id,ep_id);        

        response.status(200).send(resBO);        
    }catch(error){
        logger.debug(`Error occured while deleting applications endpoints`,`DELETE /:app_id` , `${transid}`, `${util.inspect(error,{compact:true,colors:true, depth: null})}`);
        response.status(getErrRespCode(error));        
        if(error.metadata){            
            response.send(error);
        }else{                        
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9901","Error while processing the request"));
        }     
    } 
});


function getErrRespCode(error:any): number{
    let respCode:number = 500;
    if(error.metadata === undefined){
        respCode = 500;
    }else if(error.metadata.status === '8153'){
        respCode= 404;
    }else{
        respCode = 500;
    }        
    return respCode;
}


/**
 * Routes Definition for health, readiness and liveness check
 */
 // Route for liveness prone
 // The kubelet kills the container and restarts it.
 router.get('/healthz',(_: any,res: any)=> {
    res.set("Content-Type","application/json; charset=utf-8");
    if(AppHealth.config_loaded && !AppHealth.reload_required)
        res.status(200);    
    else{
        logger.error(`Health check fail. Config Loaded: ${AppHealth.config_loaded} and Express Loaded: ${AppHealth.express_loaded}`);
        res.status(503);    
    }
        
    res.send();
});


// Route for rediness check. 
// This route will return 200 in-case all required bootstarap is finished
// Note: We want to suspend traffic in-case there is something wrong here
router.get('/readiness',(_: any,res: any)=> {
    res.set("Content-Type","application/json; charset=utf-8");
    if(AppHealth.config_loaded)
        res.status(200);    
    else{
        logger.error(`Readiness check fail. Config Loaded: ${AppHealth.config_loaded} and Express Loaded: ${AppHealth.express_loaded}`);
        res.status(503);        
    }        
    res.send();
});


// Protect slow starting containers with startup probes
router.get('/startup',(_: any,res: any)=> {
    res.set("Content-Type","application/json; charset=utf-8");
    if(AppHealth.config_loaded && AppHealth.express_loaded){
        res.status(200);    
    }else{
        logger.error(`Startup check fail. Config Loaded: ${AppHealth.config_loaded} and Express Loaded: ${AppHealth.express_loaded}`);        
        res.status(503);    
    }        
    res.send();
});


export default router;