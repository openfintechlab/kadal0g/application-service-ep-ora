import Joi                              from "joi";
import logger                           from "../../utils/Logger"
import {PostRespBusinessObjects}        from "../bussObj/Response";
import AppConfig                        from "../../config/AppConfig";

export default class ApplicationServiceEndpointValidator{
    ApplicationServiceEndpointSchemaModel = Joi.object({
        "application-service-ep":{
            "application_id": Joi.string().min(8).max(56).required(),
            "service_id": Joi.string().min(8).max(56).required(),
            "version_id": Joi.string().min(8).max(56).required(),
            "endpoint_id": Joi.string().min(8).max(56).required()
        }
    });

    ApplicationServiceParamsModel = Joi.object({        
        srv_id: Joi.string().min(8).max(56).optional(),
        ver_id: Joi.string().min(8).max(56).optional(),
        ep_id:  Joi.string().min(8).max(56).optional()
    });

    constructor(private referenceID: string){}

    public validate(request:any){
        let {error, value} = this.ApplicationServiceEndpointSchemaModel.validate(request, { presence: "required" } );
        if(error){
            logger.error(`Error while validating the business object: ${error.message}`);
            this.getValidationError(error);
        }else{
            return value;
        }
    }

    public validateParams(params:any){
        let {error, value} = this.ApplicationServiceParamsModel.validate(params, { presence: "required" } );
        if(error){
            logger.error(`Error while validating the business object`, ` ${error.message}`);
            this.getValidationError(error);
        }else{
            return value;
        }
    }

    private getValidationError(error:any){
        if(AppConfig.config.OFL_MED_NODE_ENV === 'debug'){
            let trace:PostRespBusinessObjects.Trace = new PostRespBusinessObjects.Trace("schema-validation",error.message);
            throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8502","Schema validation error", trace);   
        }else{
            throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8502","Schema validation error");   
        }           
    }
}